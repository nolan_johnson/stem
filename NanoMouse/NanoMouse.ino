#include <Servo.h>
#include "NanoMouseMotors.h"
#include "NanoMouseSensors.h"

NanoMouseMotors motors;

//left emmiter,left sensor,front,right
NanoMouseSensors<4,A7,3,A6,2,A5> sensors;

#define RIGHT 1
#define LEFT -1

const byte ledPin = 13;
const byte buttonPin = 9;

void setup()
{
  motors.attach(6,5);

  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  sensors.configure();

  Serial.begin(9600);

  motors.stop();

  while (digitalRead(buttonPin))
  {}  
}

void loop()
{  
  sensors.view();
  delay(100);
}
