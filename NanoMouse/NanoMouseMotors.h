#include <Arduino.h>
#include <Servo.h>

#define RIGHT 1
#define LEFT -1

class NanoMouseMotors {

  private:
    Servo leftServo;
    Servo rightServo;

    static const byte power = 250;

  public:

    void attach(byte leftMotor, byte rightMotor)
    {
      leftServo.attach(6);
      rightServo.attach(5);
    }
    void forward()
    {
      leftServo.writeMicroseconds(1500 - power);
      rightServo.writeMicroseconds(1500 + power);
    }

    void stop(int time = 500)
    {
      leftServo.writeMicroseconds(1500);
      rightServo.writeMicroseconds(1500);
      delay(time);
    }

    void forwardTime(unsigned int time)
    {
      forward();
      delay(time);
      stop();
    }

    void turn(int direction, int degrees)
    {
      leftServo.writeMicroseconds(1500 + power * direction);
      rightServo.writeMicroseconds(1500 + power * direction);
      delay(degrees * 5);
      stop();

    }

    void square()
    {
      for (int x = 0; x < 4; x++) {

        forwardTime(1000);
        turn(LEFT, 90);

      }
    }
};