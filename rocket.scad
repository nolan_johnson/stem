height=70;
finWidth=3;
fins=4;
finLength=30;

difference()
{
cylinder(r=63,h=height,$fn=0);
cylinder(r=60,h=height,$fn=0);
}

////////////////////////////////////////////////////////////
//negative y fin

for(x=[1:fins])
	rotate([0,0,x*360/fins])
		translate([0,-63,0])
			rotate([90,0,270])
				linear_extrude(height =finWidth, center = true, convexity = 10, twist = 0)
					polygon(points = [[0,0],[finLength,0],[0,height]]);